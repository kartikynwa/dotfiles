;; EXPERIMENTAL

(defun nyaa-beginning-of-thing (thing)
  "Move point to the beginning of THING."
  (interactive (list (meow-thing-prompt "Beginning of: ")))
  (save-window-excursion
    (let ((back (equal 'backward (meow--thing-get-direction 'beginning)))
          (bounds (meow--parse-inner-of-thing-char thing)))
      (when bounds
	(goto-char (if back (car bounds) (point)))))))


(defun nyaa-end-of-thing (thing)
  "Move point to the end of THING."
  (interactive (list (meow-thing-prompt "End of: ")))
  (save-window-excursion
    (let ((back (equal 'backward (meow--thing-get-direction 'end)))
          (bounds (meow--parse-inner-of-thing-char thing)))
      (when bounds
        (goto-char (if back (point) (cdr bounds)))))))

(defun nyaa-expand-selection-to-whole-lines ()
  "Expand selection to include the entirety of lines on which it begins and ends"
  (interactive)
  (let ((init-point (point))
        (init-mark (mark)))
    (when init-mark
      (let ((mark-is-ahead (> init-mark init-point)))
	(goto-char init-mark)
	(if mark-is-ahead (end-of-line) (beginning-of-line))
	(push-mark nil nil)
	(goto-char init-point)
	(if mark-is-ahead (beginning-of-line) (end-of-line))
	))))

;;


(defmacro nyaa--call-with-prefix (prefix form)
  `(let ((current-prefix-arg ,prefix))
     (call-interactively ,form)))

(defun nyaa-find-negative ()
  "'nyaa-find but backwards"
  (interactive)
  (nyaa--call-with-prefix -1 'meow-find))

(defun nyaa-till-negative ()
  "'nyaa-till but backwards"
  (interactive)
  (nyaa--call-with-prefix -1 'meow-till))

(defun nyaa-yank-prefixed ()
  "'yank with C-u"
  (interactive)
  (nyaa--call-with-prefix '(4) 'yank))

(defun nyaa-append-beginning-of-line ()
  "Append to the end of the current line"
  (interactive)
  (meow-beginning-of-thing ?l)
  (meow-insert))

(defun nyaa-append-end-of-line ()
  "Append to the end of the current line"
  (interactive)
  (meow-end-of-thing ?l)
  (meow-append))

(defun nyaa-quit ()
  "Quit current window if it is not the only window."
  (interactive)
  (if (> (seq-length (window-list (selected-frame))) 1)
      (quit-window)))

(defun meow-setup ()
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   ;; SPC j/k will run the original command in MOTION state.
   '("j" . "H-j")
   '("k" . "H-k")
   ;; Use SPC (0-9) for digit arguments.
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . nyaa-beginning-of-thing)
   '("]" . nyaa-end-of-thing)
   '("a" . meow-append)
   '("o" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("d" . meow-delete)
   '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . meow-next-symbol)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   ;; '("g" . nyaa-beginning-of-thing)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '("O" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . meow-join)
   '("n" . meow-search)
   ; '("o" . meow-block)
   ; '("O" . meow-to-block)
   '("p" . nyaa-yank-prefixed)
   '("P" . meow-yank)
   ;; '("q" . meow-quit)
   '("Q" . meow-goto-line)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-visit)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . nyaa-expand-selection-to-whole-lines)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '("F" . nyaa-find-negative)
   '("T" . nyaa-till-negative)
   '("I" . nyaa-append-beginning-of-line)
   '("A" . nyaa-append-end-of-line)
   '("<escape>" . meow-cancel-selection)))

(use-package meow
  :ensure t
  :config
  (meow-setup)
  (meow-global-mode 1))
